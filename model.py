# -*- coding: utf-8 -*-
"""
Model Module.

An abstract module describing the required functionality for a model.
Needs to be instantiated.

@author: ymiche
@version: 0.1
"""

import abc


class Model(object):

    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def __init__(self):
        """
        Required init function.
        """
        raise NotImplementedError

    @abc.abstractmethod
    def train(self, inputs, outputs):
        """
        Required model training function.
        """
        raise NotImplementedError

    @abc.abstractmethod
    def test(self, inputs):
        """
        Required model testing function.
        """
        raise NotImplementedError
