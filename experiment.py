# -*- coding: utf-8 -*-
"""
Experiments module.

@author: ymiche
@version: 0.1
"""

import os.path
import numpy as np
import copy

from model import Model
from error import Error, error_averager


class Experiment(object):
    """
    An experiment, defined as using a list of models on a certain dataset.
    Requires a model, a dataset, and a certain division of the data for the
    training/testing, as well as the number of rounds for the evaluation.

    The models have to be instances of the class Model (or a sub-class of it).
    The dataset is expected to be a filename, with space separated values, the
    first column being the output, the other columns being features and rows
    being samples.
    The perc_train describes how much of the data is to be used for the
    training. The rest is set for test.

    TODO : Account for the stratification

    """

    def __init__(self, models,
                 dataset, exp_info='',
                 perc_train=0.67, rounds=10):
        """
        Verifies the given arguments.
        """
        # Is the model list given of the proper class
        self.__check_models_list(models)
        self.models = models

        # Is the dataset an existing filename, and structure checks
        # Affects the self with the raw data set
        self.raw_data = self.__check_dataset(dataset)
        self.raw_shape = self.raw_data.shape
        # self.raw_data = self.raw_data.ravel()

        # Checks the exp_info for structure and content
        self.__check_info(exp_info)
        self.exp_info = exp_info

        # Check the percentage value
        self.__check_perc_train(perc_train)
        self.perc_train = perc_train

        # Use it to determine the size of the data splits
        self.training_data_shape = (int(self.raw_shape[0]*self.perc_train),
                                    self.raw_shape[1])
        self.testing_data_shape = (self.raw_shape[0] -
                                   self.training_data_shape[0] - 1,
                                   self.raw_shape[1])

        # Check the number of rounds
        self.__check_number_rounds(rounds)
        self.rounds = rounds

        # Initialize the RNG state for the experiment
        self.rng_state = None

    def __check_dataset(self, dataset):
        """
        Checks for the dataset given. Verifies the existence of the file
        and then the structure of it.
        Returns the raw input and output directly parsed from the data.
        """
        if not os.path.isfile(dataset):
            raise ValueError('Given dataset is not a file (or could not \
                              be found.)')
        # Dataset is a file, try to load and parse it
        try:
            raw_data = np.genfromtxt(dataset)
        except:
            raise IOError('Could not read dataset.')

        return raw_data

    def __check_info(self, info):
        """
        Checks the given info for structure and content.
        """
        if info is None:
            raise ValueError('Given info is None. Requires information for \
                              the experiment description.')
        if not isinstance(info, str):
            raise TypeError('Given info is not a string (got \
                             {}).'.format(type(info)))
        if info == '':
            raise ValueError('Given info is empty.')

    def __check_perc_train(self, perc_train):
        """
        Checks the percentage to be used for the training.
        Only raises Exceptions.
        """
        if not isinstance(perc_train, float):
            raise TypeError('Given training data percentage of wrong type \
                             (got {}).'.format(type(perc_train)))
        if perc_train <= 0 or perc_train >= 1:
            raise ValueError('Given training data percentage not in \
                              ]0,1[ (got {})'.format(perc_train))

    def __check_number_rounds(self, rounds):
        """
        Checks the number of rounds for structure.
        Only raises Exceptions.
        """
        if not isinstance(rounds, int):
            raise TypeError('Given number of rounds is not an integer \
                             (got {}).'.format(type(rounds)))
        if rounds < 1:
            raise ValueError('Given number of rounds is not larger than 1 \
                              (got {}).'.format(rounds))

    def __check_models_list(self, models):
        """
        Checks the models list for structure.
        Only raises Exceptions.
        """
        if not isinstance(models, list):
            raise TypeError('Given list of models is not a list \
                             (got {}).'.format(type(models)))
        if len(models) < 1:
            raise ValueError('Given list of models is empty.')
        for model in models:
            if not isinstance(model, Model):
                raise TypeError('Given model in the list is not of Model \
                                 type (got {}).'.format(type(model)))

    def run_experiment(self, rng_state=None):
        """
        Runs the training and the testing of the model on the data.
        If a random generator state is given, use it for the splits of the
        data. Otherwise, use the current one.
        RNG state is stored in the self.rng_state variable.
        Returns a dictionary of results for the experiment, to be used with
        the results printer module, for example.
        """
        # Check the rng state and store it
        if rng_state is not None:
            # Check that the rng_state is a tuple
            if not isinstance(rng_state, tuple):
                raise TypeError('Given RNG state is not a tuple \
                                 (got {}).'.format(type(rng_state)))
            self.rng_state = rng_state

        else:
            self.rng_state = np.random.get_state()

        np.random.set_state(self.rng_state)

        # Prepare the dict holding the results and errors
        training_errors_dict = {model: [] for model in self.models}
        testing_errors_dict = {model: [] for model in self.models}

        # Loop over the number of rounds to make
        for _ in xrange(self.rounds):

            # Generate a view of the data according to the splits we want
            training_inputs = self.raw_data[:self.training_data_shape[0],
                                    1:].reshape((self.training_data_shape[0],
                                               self.training_data_shape[1]-1))
            training_outputs = self.raw_data[:self.training_data_shape[0], 0].reshape((self.training_data_shape[0], 1))

            testing_inputs = self.raw_data[self.training_data_shape[0]+1:, 1:].reshape((self.testing_data_shape[0], self.testing_data_shape[1]-1))
            testing_outputs = self.raw_data[self.training_data_shape[0]+1:, 0].reshape((self.testing_data_shape[0], 1))

            # Generate a permutation of the raw data
            np.random.shuffle(self.raw_data)

            # Normalize that view (generates a copy)
            training_inputs, means, stds = self.normalize(training_inputs)
            testing_inputs, means, stds = self.normalize(testing_inputs, means, stds)

            # Loop over all the models to test
            for model in self.models:
                # Run the training part. Returns an error object and the
                # trained model
                temp_model = copy.deepcopy(model)
                training_error, temp_model = self._run_training(temp_model,
                                                           training_inputs,
                                                           training_outputs)
                training_errors_dict[model].append(training_error)
                testing_error = self._run_testing(temp_model,
                                                  testing_inputs,
                                                  testing_outputs)
                testing_errors_dict[model].append(testing_error)

        results_dict_list = self.gen_results_dict_list(training_errors_dict,
                                                       testing_errors_dict)
        return results_dict_list

    def gen_results_dict_list(self, training_errors_dict, testing_errors_dict):
        """
        Generates a list of dictionaries of results, as expected per the
        results printer module.
        training_errors and testing_errors are to be dictionaries holding
        lists of errors for each round, as generated by the run_experiment
        function.
        Returns a list of dictionaries.
        """
        results_dict_list = []

        if len(training_errors_dict) != len(testing_errors_dict):
            raise ValueError('Given dictionaries do not have the same length \
                              (training_errors is {} long, testing_errors is \
                              {} long).'.format(len(training_errors_dict),
                                                len(testing_errors_dict)))

        for model in training_errors_dict:
            results_dict = {'exp_info': self.exp_info,
                            'model_info': model.model_info,
                            'training': {},
                            'testing': {}
                            }
            results_dict['training'] = error_averager(training_errors_dict[model])
#            print 'Training:'
#            for i in xrange(len(training_errors_dict[model])):
#                print training_errors_dict[model][i]
#            print 'Testing:'
#            for i in xrange(len(training_errors_dict[model])):
#                print testing_errors_dict[model][i]
            results_dict['testing'] = error_averager(testing_errors_dict[model])

            results_dict_list.append(results_dict)

        return results_dict_list

    def normalize(self, data_matrix, means=None, stds=None):
        """
        Normalizes the data using the given normalization factors, if any.
        If no means and stds are given, the data is normalized to 0-mean and
        unit std.
        Returns the normalized data, and the means and stds computed or given.
        """
        if means is not None:
            if not isinstance(means, np.ndarray):
                raise TypeError('Given array of means is not a numpy array \
                                (got {}).'.format(type(means)))
            if stds is not None:
                if not isinstance(stds, np.ndarray):
                    raise TypeError('Given array of stds is not a numpy \
                                     array (got {}).'.format(type(stds)))
                # We have both normalization factors, normalize using them
                # Check for size
                if len(means) != data_matrix.shape[1]:
                    raise ValueError('Given array of means has the wrong \
                                      size (got {}, expected \
                                      {}).'.format(len(means),
                                                   data_matrix.shape[1]))
                if len(stds) != data_matrix.shape[1]:
                    raise ValueError('Given array of stds has the wrong \
                                      size (got {}, expected \
                                      {}).'.format(len(stds),
                                                   data_matrix.shape[1]))

        else:
            # Means is empty, we want to normalize to 0-mean and unit std
            means = np.mean(data_matrix, axis=0)
            stds = np.std(data_matrix, axis=0)

        data_matrix = data_matrix - np.tile(means, (data_matrix.shape[0], 1))
        data_matrix = data_matrix/np.tile(stds, (data_matrix.shape[0], 1))
        return data_matrix, means, stds

    def _run_training(self, model, inputs, outputs):
        """
        Runs the training of the model.
        Returns an error object and the trained model.
        """
        estimated_outputs = model.train(inputs, outputs)
        training_error = Error(estimated_outputs, outputs)
        return training_error, model

    def _run_testing(self, model, inputs, outputs):
        """
        Runs the test of the model.
        Returns an error object.
        """
        estimated_output = model.test(inputs)
        testing_error = Error(estimated_output, outputs)
        return testing_error
