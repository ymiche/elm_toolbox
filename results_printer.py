# -*- coding: utf-8 -*-
"""
Results Printer Module.

Makes a "pretty" output out of the given dictionary of values.

@author: ymiche
@version: 0.1
"""


class Results_Printer(object):
    """
    Results printer class.
    """

    def __init__(self, results_dict_list):
        """
        Checks for the dictionaries of results in the list.
        """
        self.__check_results_dict(results_dict_list)
        self.results_dict_list = results_dict_list

    def __check_results_dict(self, results_dict_list):
        """
        Checks the dictionaries of results for the correct keys.
        Each entry in the list must have the following structure:
            {'exp_info': 'str holding details of the experiment',
             'model_info': 'str with short extra info on the model used',
             'training': {'ERRNAME': (mean, std),
                          'ERRNAME': (mean, std),
                          ...},
             'testing': {'ERRNAME': (mean, std),
                         ...}
            }
        """
        if not isinstance(results_dict_list, list):
            raise TypeError('''Given results dictionaries not a list
                             (got {})'''.format(type(results_dict_list)))
        if len(results_dict_list) == 0:
            raise ValueError('Given list of results is empty.')
        for results_dict in results_dict_list:
            if not isinstance(results_dict, dict):
                raise TypeError('Given results dictionary is not a dict \
                                 (got {})'.format(type(results_dict)))
            if 'exp_info' not in results_dict:
                raise ValueError('Given results dictionary lacks info key.')
            if 'training' not in results_dict:
                raise ValueError('Given results dictionary lacks training \
                                  key.')
            if 'model_info' not in results_dict:
                raise ValueError('Given results dictionary lacks model \
                                  info key.')
            if 'testing' not in results_dict:
                raise ValueError('Given results dictionary lacks testing key.')

    def display(self):
        """
        A pretty output of the dictionary of results.
        """

        print '''Results for
                   {}'''.format(self.results_dict_list[0]['exp_info'])

        print 'Training:'

        for results_dict in self.results_dict_list:

            temp_str = '\t {}:'.format(results_dict['model_info'])
            for err_name in results_dict['training']:
                temp_str += '\t {}: {:e}, {:e} '.format(err_name,
                                        results_dict['training'][err_name][0],
                                        results_dict['training'][err_name][1])
            print temp_str

        print 'Testing:'

        for results_dict in self.results_dict_list:

            temp_str = '\t {}:'.format(results_dict['model_info'])
            for err_name in results_dict['testing']:
                temp_str += '\t {}: {:e}, {:e} '.format(err_name,
                                        results_dict['testing'][err_name][0],
                                        results_dict['testing'][err_name][1])
            print temp_str
        print '\n'
