Extreme Learning Machine Toolbox
================================

Some currently personal code for various ELM based models and implementations.



This software is released under the New BSD License, available in the file LICENSE.md of which you should have received a copy along with this software.
