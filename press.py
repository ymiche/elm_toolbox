# -*- coding: utf-8 -*-
"""
Prediction Sum of Squares functions.

From:

@author: ymiche
@version: 0.1
"""

import numpy as np
from numpy.linalg import inv
from scipy.optimize import minimize


class PRESS(object):

    def __init__(self, X, Y, regularization=None):
        """
        Initialize a PRESS object and perform some rough checking.
        """
        self.__check_data_consistency(X)
        self.X = X
        self.N, _ = X.shape
        self.__check_data_consistency(Y)
        self.Y = Y
        self.__check_regularization(regularization)
        self.regularization = regularization

    def __check_data_consistency(self, data_matrix):
        """
        Basic checks for the data consistency.
        """
        if not isinstance(data_matrix, np.ndarray):
            raise TypeError('Given data is not a Numpy array \
                            (got {}).'.format(type(data_matrix)))
        if not len(data_matrix.shape) == 2:
            raise ValueError('Given data is not 2-dimensional \
                             (got {}).'.format(data_matrix.shape))

    def __check_regularization(self, regularization):
        """
        Basic checks for the regularization parameter.
        """
        if not isinstance(regularization, (int, float, np.double, type(None))):
            raise TypeError('Given regularization is not a number \
                            (got {}).'.format(type(regularization)))

    def compute_MSE(self):
        """
        Compute the Prediction Sum of Squares (PRESS) for the given inputs.
        """
        # The case of the regularized PRESS
        if self.regularization is None:
            C = inv(np.dot(self.X.T, self.X))
            P = self.X.dot(C)
            W = C.dot(self.X.T).dot(self.Y)
            D = np.ones((self.N, )) - np.einsum('ij,ji->i', P, self.X.T)
            e = (self.Y - self.X.dot(W)) / D.reshape((-1, 1))
            self.MSE = np.mean(e**2)
        else:
            U, S, V = np.linalg.svd(self.X, full_matrices=False)
            A = np.dot(self.X, V.T)
            B = np.dot(U.T, self.Y)

            # function for optimization
            def regularization_opt(regularization, S, A, B, U, Y, N):
                Sd = S**2 + regularization
                C = A*(S/Sd)
                P = np.dot(C, B)
                D = np.ones((N,)) - np.einsum('ij,ji->i', C, U.T)
                e = (Y - P) / D.reshape((-1, 1))
                MSE = np.mean(e**2)
                return MSE

            res = minimize(regularization_opt, self.regularization, args=(S, A, B, U, self.Y, self.N), method="Powell")
            if not res.success:
                print "Lambda optimization failed:  (using basic results)"
                print res.message
                self.MSE = regularization_opt(self.regularization, S, A, B, U, self.Y, self.N)
                self.regularization_optimised = None
            else:
                self.regularization_optimised = res.x
                self.MSE = res.fun
