# -*- coding: utf-8 -*-
"""
Data generation Module.

THIS MODULE IS UNFINISHED.

@author: ymiche
@version: 0.1
"""

import numpy as np


def genFriedmanData(numSamples=1000):
    """
    Generates so-called Friedman data.
    """
    x = np.hstack((np.random.rand(numSamples, 5),
                   np.random.randn(numSamples, 5)))
    friedman = lambda x: 10 * np.sin(np.pi * x[:, 0] * x[:, 1]) + \
        20 * (x[:, 2] + 0.5) ** 2 + \
        10 * x[:, 3] + \
        5 * x[:, 4] + \
        (x[:, 5] + x[:, 6] + x[:, 7] + x[:, 8] + x[:, 9])
    y = friedman(x)
    y = np.array(y).reshape((len(y), 1))
    xtrain = x[:int(np.floor(numSamples * .8)), :]
    ytrain = y[:int(np.floor(numSamples * .8)), :]
    xtest = x[int(np.floor(numSamples * .8)):, :]
    ytest = y[int(np.floor(numSamples * .8)):, :]
    return xtrain, ytrain, xtest, ytest
