# -*- coding: utf-8 -*-
"""
Mathematical and various functions.

@author: ymiche
@version: 0.1
"""

import numpy as np


def sigmoid(z):
    """
    The sigmoid function.
    """
    s = 1.0 / (1.0 + np.exp(-1.0 * z))
    return s
