# -*- coding: utf-8 -*-
"""
Base ELM Module.
Abstract class for the ELM. Needs to be instantiated.

@author: ymiche
@version: 0.1
"""

import numpy as np
import abc
from collections import OrderedDict

from model import Model
from mathfunctions import sigmoid


# Typical ELM with linear neurons added.
class ELM(Model):

    # We need these equivalences for the activation functions
    ACTIVATION_FUNCTIONS = {'tanh': np.tanh,
                            'sigmoid': sigmoid,
                            'sine': np.sin,
                            'linear': lambda x: x}

    def __init__(self, neurons={'tanh': 100,
                                'sigmoid': 0,
                                'sine': 0,
                                'linear': True},
                 gen_p_function=np.random.randn):
        # Argument Checking
        # The structure of neurons to use
        # Fixed to an ordered dict so as to keep the order of the neurons
        self.neurons = self.__check_neurons(neurons)

        self.__check_gen_p_function(gen_p_function)
        # The input matrix generating function
        self.gen_p_function = gen_p_function

        # Whether the model has been trained already
        self.trained = False

        # The input biases and weights, which get their value in the
        # train function
        self.input_biases = None
        self.input_weights = None
        # The output weights, also getting their value in the train function
        self.output_weights = None

    def __check_neurons(self, neurons):
        """
        Basic checks for the structure of the neurons.
        """
        if not isinstance(neurons, dict):
            raise TypeError('Given structure of neurons is not a dict \
                            (got {}).'.format(type(neurons)))

        returned_neurons = OrderedDict()
        total_number_neurons = 0
        for key in neurons:
            if key != 'linear':
                if key not in self.ACTIVATION_FUNCTIONS:
                    raise ValueError('Given structure of neurons not of \
                                      appropriate type (got {}).'.format(key))
                if not isinstance(neurons[key], (int, bool)):
                    raise TypeError('Given number of neurons is not an \
                                     integer (got \
                                     {}).'.format(type(neurons[key])))
                if neurons[key] < 0:
                    raise ValueError('Given number of neurons is not \
                                      a positive integer (got \
                                      {}).'.format(neurons[key]))
                total_number_neurons += neurons[key]
                returned_neurons[key] = neurons[key]

        self.number_neurons = total_number_neurons
        # The degenerate case of the purely linear ELM, not an ELM anymore...
        if (total_number_neurons <= 0) and not neurons['linear']['present']:
            raise ValueError('Given total number of neurons is not strictly \
                              positive (got {}).'.format(total_number_neurons))

        # Take care of the special linear neurons
        if 'linear' in neurons:
            if neurons['linear']:
                returned_neurons['linear'] = {'present': True,
                                              'number': 0,
                                              'list': []}
            else:
                returned_neurons['linear'] = {'present': False}

        return returned_neurons

    def __check_data_consistency(self, data_matrix):
        """
        Basic checks for the data consistency.
        """
        if not isinstance(data_matrix, np.ndarray):
            raise TypeError('Given data is not a Numpy array \
                            (got {}).'.format(type(data_matrix)))
        if not len(data_matrix.shape) == 2:
            raise ValueError('Given data is not 2-dimensional \
                             (got {}).'.format(data_matrix.shape))

    def __check_gen_p_function(self, gen_p_function):
        """
        Basic check for the weight generation function.
        """
        if not hasattr(gen_p_function, '__call__'):
            raise TypeError('Given generating function is not callable.')

    # Abstract methods
    @abc.abstractmethod
    def train(self, inputs, outputs):
        """
        Required training function for the ELM.
        """
        raise NotImplementedError

    @abc.abstractmethod
    def test(self, inputs):
        """
        Required testing function for the ELM.
        """
        raise NotImplementedError
