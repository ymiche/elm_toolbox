# -*- coding: utf-8 -*-
"""
Scratch code for the experiments testing.

@author: ymiche
@version: 0.1
"""

from experiment import Experiment
from gbhelm import GBHELM
from opelm import OPELM
#from rotelm1 import ROTELM
#from rotelm2 import ROTELM2
from results_printer import Results_Printer


def main():
    """
    Run the desired experiments.
    """
    elm1 = GBHELM(neurons={'tanh': 151,
                           'sine': 0,
                           'sigmoid': 0,
                           'linear': True})
    elm3 = GBHELM(neurons={'tanh': 150,
                           'sine': 0,
                           'sigmoid': 0,
                           'linear': False})

    opelm1 = OPELM(neurons={'tanh': 100,
                            'sine': 0,
                            'sigmoid': 0,
                            'linear': True})

    #rotelm1 = ROTELM(number_neurons=50, linear_neurons=True)

    # rotelm2 = ROTELM2(number_neurons=50, linear_neurons=True)

#    experiment_1 = Experiment(
#        [elm1, elm3, opelm1],
#        './california_housing.txt',
#        exp_info='Scratch Experiment on Housing',
#        rounds=10)

    experiment_2 = Experiment(
        [opelm1, elm1, elm3],
        './abalone.txt',
        exp_info='Scratch Experiment on Abalone',
        rounds=50)

    experiment_3 = Experiment(
        [opelm1, elm1, elm3],
        './ailerons.txt',
        exp_info='Scratch Experiment on Ailerons',
        rounds=50)

    experiment_4 = Experiment(
        [opelm1, elm1, elm3],
        './elevators.txt',
        exp_info='Scratch Experiment on Elevators',
        rounds=50)

#    results1 = experiment_1.run_experiment()
    results2 = experiment_2.run_experiment()
    results3 = experiment_3.run_experiment()
    results4 = experiment_4.run_experiment()

#    my_result_printer1 = Results_Printer(results1)
#    my_result_printer1.display()

    my_result_printer2 = Results_Printer(results2)
    my_result_printer2.display()

    my_result_printer3 = Results_Printer(results3)
    my_result_printer3.display()

    my_result_printer4 = Results_Printer(results4)
    my_result_printer4.display()

if __name__ == '__main__':
    main()
