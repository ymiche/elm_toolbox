# -*- coding: utf-8 -*-
"""
An implementation of the Multi-Response Sparse Regression algorithm (MRSR).
This algorithm is an extension of the more common Least Angle Regression
(LARS), itself an extension of the LASSO.

From Timo Similä and Jarkko Tikka,
"Common subset selection of inputs in multiresponse regression",
International Joint Conference on Neural Networks, 2006

@author: Anton Akusok (some alterations by Yoan Miche)
@version: 0.1
"""

import numpy as np
from scipy import optimize
from scipy.linalg import cho_factor, cho_solve, solve


# TODO : Rewrite this code properly...

class MRSR (object):

    def __init__(self, X, T, norm=1):
        """
        Builds linear model.

        X is input data.
        T are real outputs (targets).
        """
        ins = X.shape[1]
        outs = T.shape[1]

        self.norm = norm
        # Fortran ordering is good for operating on columns
        self.X = np.array(X, order='F')
        self.XA = np.empty(X.shape, order='F')
        self.T = T
        self.XX = np.dot(X.T, X)
        self.XT = np.dot(X.T, T)

        # active inputs list
        self.A = []
        self.nA = range(ins)
        # current projection estimator
        self.W = np.zeros((ins, outs))
        # current target estimator
        self.Y = np.zeros(T.shape)
        # currently added input dimension
        self.j_current = None

    #@profile
    def new_input(self):
        """
        Adds one input to a current model using a variable length step.
        """

        # first step
        if len(self.A) == 0:
            c_max = -1
            for j in self.nA:
                c_kj = np.linalg.norm(np.dot(self.T.T, self.X[:, j]), self.norm)
                if c_kj > c_max:
                    c_max = c_kj
                    j_max = j
            self.j_current = j_max
            # save new input
            self.A.append(self.j_current)
            self.nA.remove(self.j_current)
            # swap columns
            idx = len(self.A)-1
            self.XA[:, idx] = self.X[:, self.j_current]
            y_min = 1

        # last step
        elif len(self.nA) == 0:
            print "last step"
            self.Y = self.Yk1
            self.W[self.A] = self.Wk1
            y_min = 1

        # intermediate step
        else:
            Yk2 = (self.Yk1 - self.Y).T
            T2 = (self.T - self.Y).T
            c_max = np.linalg.norm(np.dot(T2, self.X[:, self.j_current]),
                                   self.norm)
            # super fast parameterized function
            fun_p = lambda y, p1, p2: (1-y)*c_max - np.linalg.norm(p1 - y*p2)

            # find optimal step (minimum over possible additional inputs)
            # upper interval
            y_min = 1
            for j_new in self.nA:
                x_new = self.X[:, j_new]
                # pre-calculate constant parts of the optimization function
                # for the given x_new
                p1 = T2.dot(x_new)
                p2 = Yk2.dot(x_new)
                # skip optimization if min(fun) > y_min
                if (1-y_min)*c_max < np.linalg.norm(p1 - y_min*p2):
                    try:
                        # finding a value greater than zero
                        zero = 1E-15
                        y_kj = optimize.brentq(fun_p, zero, y_min,
                                               xtol=1E-6, args=(p1, p2))
                        y_min = y_kj
                        j_min = j_new
                    except ValueError:
                        # ValueError: f(a) and f(b) must have different signs
                        # here f(a) < 0 and f(b) < 0; does not fit our
                        # purposes anyway
                        # ignoring this case
                        pass

            # if no suitable solution was found
            if y_min == 1:
                j_min = j_new
            self.j_current = j_min

            # add new input into model
            self.A.append(self.j_current)
            self.nA.remove(self.j_current)
            # add new input to X matrix
            idx = len(self.A)-1
            self.XA[:, idx] = self.X[:, self.j_current]

        # post-update ELM estimation with current set of inputs, with LU-ELM
        XtX = self.XX[self.A, :][:, self.A]
        XtT = self.XT[self.A, :]

        self.Wk1 = solve(XtX, XtT)

        # replace fancy indexing with simple one
        X = self.XA[:, :len(self.A)]
        self.Yk1 = np.dot(X, self.Wk1)

        if len(self.A) > 1:
            # perform variable length step
            self.Y = (1-y_min)*self.Y + y_min*self.Yk1
            self.W = (1-y_min)*self.W
            self.W[self.A] += y_min*self.Wk1

        return y_min

    def get_ols_solution(self):
        """
        Return OLS solution for a current model.

        Current estimates of Y and W are useful in the model,
        but are very bad for prediction.
        """
        X = self.XA[:, :len(self.A)]
        XtX = self.XX[self.A, :][:, self.A]
        XtT = self.XT[self.A, :]

        # cholesky solution is more stable
        LLt = cho_factor(XtX)
        Wk1 = cho_solve(LLt, XtT)

        Yk1 = np.dot(X, Wk1)
        return Wk1, Yk1
























































