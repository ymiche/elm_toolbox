# -*- coding: utf-8 -*-
"""
OP-ELM Module.

From Yoan Miche; Sorjamaa, A; Bas, P.; Simula, O.; Jutten, C.; Lendasse, A,
"OP-ELM: Optimally Pruned Extreme Learning Machine,"
Neural Networks, IEEE Transactions on , vol.21, no.1, pp.158,162, Jan. 2010
doi: 10.1109/TNN.2009.2036259

@author: ymiche
@version: 0.1
"""


import numpy as np

from elm import ELM
from mrsr import MRSR
from press import PRESS


class OPELM(ELM):
    """
    OPELM, with linear neurons added.
    """

    # This constant defines how many neurons are tested without improvements
    # before the system stops and gives the final pruning
    MAX_IMPROVEMENT_WAIT = 50

    def __init__(self, neurons={'tanh': 100,
                                'sine': 0,
                                'sigmoid': 0,
                                'linear': True},
                 gen_p_function=np.random.randn):
        ELM.__init__(self, neurons=neurons,
                     gen_p_function=gen_p_function)
        self.model_info = 'OPELM with {} neurons.'.format(self.number_neurons)

    def train(self, inputs, outputs):
        """
        Basic OPELM training function.
        """
        # Some basic checks on the data
        self._ELM__check_data_consistency(inputs)
        self._ELM__check_data_consistency(outputs)
        if not outputs.shape[1] == 1:
            raise Exception('Given output is multi-dimensional, not handled.')
        if not inputs.shape[0] == outputs.shape[0]:
            raise Exception('Given input and output do not have the same \
                number of samples')

        # Generate random input weights according to the generating function
        self.input_weights = self.gen_p_function(self.number_neurons,
                                                 inputs.shape[1])
        # Generate random input biases
        self.input_biases = self.gen_p_function(self.number_neurons, 1)
        # Project the input data
        hidden_layer = np.dot(inputs, self.input_weights.T)
        # Add the biases
        hidden_layer += np.tile(self.input_biases,
                                (1, hidden_layer.shape[0])).T
        # Non-linearity in the neurons (tanh function)
        # TODO : improve this, could be much faster using a cython function...
        acc = 0
        for neuron_type in self.neurons:
            if neuron_type != 'linear':
                hidden_layer[:, acc:self.neurons[neuron_type]] = self.ACTIVATION_FUNCTIONS[neuron_type](hidden_layer[:, acc:self.neurons[neuron_type]])
                acc += self.neurons[neuron_type]
        # Add linear neurons to the total, if required
        # They always come at the end of the stack of neurons
        if self.neurons['linear']['present']:
            hidden_layer = np.hstack((hidden_layer, inputs))

        # Get a ranking of the neurons with the MRSR (LARS)
        mrsr_object = MRSR(hidden_layer, outputs, norm=2)
        for _ in xrange(hidden_layer.shape[1]):
            mrsr_object.new_input()
        ranked_features = np.array(mrsr_object.A)

        # Evaluate the error for each combination
        # Use the PRESS LOO for it
        lowest_err = np.inf
        counter = 0
        for i in xrange(len(ranked_features)):
            if i == len(ranked_features) - 1:
                # In this case we have reached the end of the features list
                argmin_features = i - counter
                break
            if counter > self.MAX_IMPROVEMENT_WAIT:
                argmin_features = i - self.MAX_IMPROVEMENT_WAIT
                break
            Hi = hidden_layer.take(ranked_features[:i+1], axis=1)
            press_object = PRESS(Hi, outputs, None)
            press_object.compute_MSE()
            counter += 1
            if press_object.MSE < lowest_err:
                lowest_err = press_object.MSE
                counter = 0

        # indices of best neurons
        best_neurons = ranked_features[:argmin_features]

        # Rank the best neurons for easier handling of the activation functions
        best_neurons.sort()
        # Prune the input and output matrix
        acc = 0
        pruned_neurons = self.neurons.copy()

        for neuron_type in self.neurons:
            # This is the amount of neurons kept for this type of activation
            # function
            # The linear neurons are handled a bit differently
            if neuron_type != 'linear':
                neurons_kept = [i for i in best_neurons if i < (acc + self.neurons[neuron_type])]
                pruned_neurons[neuron_type] = len(neurons_kept)
            else:
                # The case of the linear neurons
                if self.neurons['linear']['present']:
                    neurons_kept = [i for i in best_neurons
                                    if i > self.number_neurons]

                    pruned_neurons['linear']['number'] = len(neurons_kept)
                    pruned_neurons['linear']['list'] = [i - self.number_neurons for i in neurons_kept]

            amount_kept = len(neurons_kept)

            if amount_kept != 0:
                # If we have not yet created the pruned hidden layer
                if 'pruned_hidden_layer' not in locals():
                    pruned_hidden_layer = np.take(hidden_layer,
                                                  neurons_kept, axis=1)
                    if neuron_type != 'linear':
                        pruned_input_weights = np.take(self.input_weights,
                                                   neurons_kept, axis=0)
                        pruned_input_biases = np.take(self.input_biases,
                                                  neurons_kept, axis=0)
                else:
                    np.hstack((pruned_hidden_layer,
                               np.take(hidden_layer, neurons_kept, axis=1)))
                    if neuron_type != 'linear':
                        np.vstack((pruned_input_weights,
                               np.take(self.input_weights,
                                       neurons_kept,
                                       axis=0)))
                        np.vstack((pruned_input_biases,
                               np.take(self.input_biases,
                                       neurons_kept,
                                       axis=0)))
            if neuron_type != 'linear':
                acc += self.neurons[neuron_type]

        # Add the linear neurons to the pruned hidden layer
        np.hstack((pruned_hidden_layer, np.take(inputs, pruned_neurons['linear']['list'], axis=1)))

        # Update the inner structure of the object
        self.neurons = pruned_neurons
        self.input_weights = pruned_input_weights
        self.input_biases = pruned_input_biases
        # And the hidden layer
        hidden_layer = pruned_hidden_layer

        # Calculate the final output nodes
        betas = np.linalg.pinv(hidden_layer)
        self.output_weights = np.dot(betas, outputs)
        self.trained = True

        predicted_output = np.dot(hidden_layer, self.output_weights)

        return predicted_output

    def test(self, inputs):
        """
        Basic OPELM testing function.
        """
        # Some basic checks on the data
        self._ELM__check_data_consistency(inputs)
        if not isinstance(inputs, np.ndarray):
            raise Exception('Given input data is not a Numpy array.')
        if not len(inputs.shape) == 2:
            raise Exception('Given input data is not 2-dimensional.')

        hidden_layer = np.dot(inputs, self.input_weights.T)
        hidden_layer += np.tile(self.input_biases,
                                (1, hidden_layer.shape[0])).T
        if self.neurons['linear']['present']:
            np.hstack((hidden_layer, np.take(inputs, self.neurons['linear']['list'], axis=1)))
        acc = 0
        for neuron_type in self.neurons:
            if neuron_type != 'linear':
                hidden_layer[:, acc:self.neurons[neuron_type]] = self.ACTIVATION_FUNCTIONS[neuron_type](hidden_layer[:, acc:self.neurons[neuron_type]])
                acc += self.neurons[neuron_type]

        predicted_output = np.dot(hidden_layer, self.output_weights)
        return predicted_output
