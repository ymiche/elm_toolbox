# -*- coding: utf-8 -*-
"""
Standard ELM Module.

From G.-B. Huang, Q.-Y. Zhu and C.-K. Siew,
“Extreme Learning Machine: Theory and Applications”,
Neurocomputing, vol. 70, pp. 489-501, 2006.

@author: ymiche
@version: 0.1
"""

import abc
import numpy as np

from elm import ELM


class GBHELM(ELM):

    __metaclass__ = abc.ABCMeta
    """
    The basic type of ELM, with linear neurons added.
    """

    def __init__(self, neurons={'tanh': 100,
                                'sine': 0,
                                'sigmoid': 0,
                                'linear': True},
                 gen_p_function=np.random.randn):
        ELM.__init__(self, neurons=neurons,
                     gen_p_function=gen_p_function)
        self.model_info = 'GBHELM with {} neurons.'.format(self.number_neurons)

    def train(self, inputs, outputs):
        """
        Basic ELM training function.
        """
        # Some basic checks on the data
        self._ELM__check_data_consistency(inputs)
        self._ELM__check_data_consistency(outputs)
        if not outputs.shape[1] == 1:
            raise Exception('Given output is multi-dimensional, not handled.')
        if not inputs.shape[0] == outputs.shape[0]:
            raise Exception('Given input and output do not have the same \
                number of samples')

        # Generate random input weights according to the generating function
        self.input_weights = self.gen_p_function(self.number_neurons,
                                                 inputs.shape[1])
        # Generate random input biases
        self.input_biases = self.gen_p_function(self.number_neurons, 1)
        # Project the input data
        hidden_layer = np.dot(inputs, self.input_weights.T)
        # Add the biases
        hidden_layer += np.tile(self.input_biases,
                                (1, hidden_layer.shape[0])).T
        # Non-linearity in the neurons (tanh function)
        # TODO : improve this, could be much faster using a cython function...
        acc = 0
        for neuron_type in self.neurons:
            if neuron_type != 'linear':
                hidden_layer[:, acc:self.neurons[neuron_type]] = self.ACTIVATION_FUNCTIONS[neuron_type](hidden_layer[:, acc:self.neurons[neuron_type]])
                acc += self.neurons[neuron_type]
        # Add linear neurons to the total, if required
        # They always come at the end of the stack of neurons
        if self.neurons['linear']['present']:
            hidden_layer = np.hstack((hidden_layer, inputs))

        betas = np.linalg.pinv(hidden_layer)
        self.output_weights = np.dot(betas, outputs)
        self.trained = True
        predicted_output = np.dot(hidden_layer, self.output_weights)
        return predicted_output

    def test(self, inputs):
        """
        Basic ELM testing function.
        """
        # Some basic checks on the data
        self._ELM__check_data_consistency(inputs)
        if not isinstance(inputs, np.ndarray):
            raise Exception('Given input data is not a Numpy array.')
        if not len(inputs.shape) == 2:
            raise Exception('Given input data is not 2-dimensional.')

        hidden_layer = np.dot(inputs, self.input_weights.T)
        hidden_layer += np.tile(self.input_biases,
                                (1, hidden_layer.shape[0])).T
        acc = 0
        for neuron_type in self.neurons:
            if neuron_type != 'linear':
                hidden_layer[:, acc:self.neurons[neuron_type]] = self.ACTIVATION_FUNCTIONS[neuron_type](hidden_layer[:, acc:self.neurons[neuron_type]])
                acc += self.neurons[neuron_type]
        if self.neurons['linear']['present']:
            hidden_layer = np.hstack((hidden_layer, inputs))
        predicted_output = np.dot(hidden_layer, self.output_weights)
        return predicted_output
