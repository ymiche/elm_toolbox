# -*- coding: utf-8 -*-
"""
Error calculation module.

Pretty display!

@author: ymiche
@version: 0.1
"""

import numpy as np


class Error(object):
    """
    The generic error calculation class.
    Calculates the error between two numpy arrays (vectors, not matrices).
    The arrays can be specified at init time, or when accessing the functions
    of the class.
    """

    def __init__(self, var1=None, var2=None):
        """
        Initialize the error object with empty errors.
        Check the input also, if any.
        """

        self.var1 = var1
        self.var2 = var2
        self.errors = {'MAE': None,
                       'MSE': None,
                       'RMSE': None
                       }

        if self.var1 is not None:
            # We have something in var1, check it
            self.__check_is_vector(self.var1)
            if self.var2 is not None:
                # We have something in var2, check it as well
                self.__check_is_vector(self.var2)
                # Check consistency between the two
                self.__check_data_match(self.var1, self.var2)
                # Both vectors are alright, compute the errors
                self.errors['MAE'] = self.compute_error(self.mae,
                                                        self.var1, self.var2)
                self.errors['MSE'] = self.compute_error(self.mse,
                                                        self.var1, self.var2)
                self.errors['RMSE'] = self.compute_error(self.rmse,
                                                         self.var1, self.var2)

    def __check_is_vector(self, var):
        """
        Checks for data type and shape.
        Only raises exceptions.
        """
        if not isinstance(var, np.ndarray):
            raise TypeError('Given data is not a Numpy array \
                             ({}).'.format(type(var)))
        if not len(var.shape) == 2:
            raise ValueError('Given data is not 2-dimensional (shape: \
                              {}).'.format(var.shape))

    def __check_data_match(self, var1=None, var2=None):
        """
        Checks for data consistency between self elements, or given ones.
        """
        if var1.shape != var2.shape:
            raise ValueError('Given vectors do not have the same structure \
                              ({} and {})'.format(var1.shape, var2.shape))

    def compute_error(self, errf, var1=None, var2=None):
        """
        General error calculation function, expects an error function that can
        compute a value out of two numpy vectors.
        If nothing is passed as argument, computes the MAE between the self
        var1 and var2 variables.
        If one vector is passed, tries to compute the MAE between the self var1
        and the passed vector.
        Does not affect the self variables in this case!
        If both are passed, computes the MAE between the two, and returns it.
        Does not affect the self variables in this case!
        """
        # Check the error function
        if not hasattr(errf, '__call__'):
            raise TypeError('Given error function is not callable.')
        # Check for the arguments as they have been passed
        if var1 is not None:
            self.__check_is_vector(var1)
            if var2 is not None:
                # This is the case where we compute the error between the two
                self.__check_is_vector(var2)
                self.__check_data_match(var1, var2)
                the_error = errf(var1, var2)
                return the_error

            else:
                # We check for the existence of a self variable to compare to
                if self.var1 is not None:
                    # And compute the error and return it
                    self.__check_data_match(var1, self.var1)
                    the_error = errf(var1, self.var1)
                    return the_error
                else:
                    # In this case, only var1 was ever given, so we raise
                    raise ValueError('No vector against which to compute the \
                                      error.')

        else:
            # Var1 is empty, we do not even look at var2, and only try to
            # compute the error between self variables
            # Check that self.var1 and self.var2 exist and are not None
            if self.var1 is not None:
                if self.var2 is not None:
                    # Compute the error between them
                    the_error = errf(self.var1, self.var2)
                    # Return it as well
                    return the_error

    def mae(self, var1, var2):
        """
        Mean Absolute Error.
        """
        return np.mean(np.abs(var1 - var2))

    def mse(self, var1, var2):
        """
        Mean Square Error.
        """
        return np.mean((var1 - var2) ** 2)

    def rmse(self, var1, var2):
        """
        Root Mean Square Error.
        """
        return np.sqrt(self.mse(var1, var2))

    def __str__(self):
        """
        Basic display of the error object. For debugging purposes.
        """
        output = ''
        for key in self.errors:
            output += '{}: {:e}\t'.format(key, self.errors[key])
        return output


def error_averager(errors_list):
    """
    Returns the average and std of the error objects in the errors_list.
    Returns it as a dict of values, with keys the names of the errors.
    """
    if not isinstance(errors_list, list):
        raise TypeError('Given list of errors is not a list \
                         (got {}).'.format(type(errors_list)))
    if len(errors_list) == 0:
        raise ValueError('Given list of errors is empty.')
    for error in errors_list:
        if not isinstance(error, Error):
            raise TypeError('Given error in the list is not of class Error.')

    errors = {}

    # Calculate the mean and std of the errors, type by type
    for error_type in errors_list[0].errors:
        error_list_by_type = [errors_list[i].errors[error_type]
                              for i in xrange(len(errors_list))]
        the_mean = np.mean(error_list_by_type)
        the_std = np.std(error_list_by_type)
        errors[error_type] = (the_mean, the_std)

    return errors
