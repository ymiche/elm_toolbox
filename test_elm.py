# -*- coding: utf-8 -*-
"""
Test Module for the elm.py module.

Runs series of tests on the data given/created.

@author: ymiche
@version: 0.1
"""


import unittest
from nose.tools import raises, assert_raises
import numpy as np
import scipy as sp
from scipy import io

from elm import Base_ELM


# Instantiate the abstract class Base_ELM for basic testing of the __init__
class Base_ELM_Instance(Base_ELM):
    """
    Define an instance of the Base_ELM for the test.
    """
    def train(self, inputs, outputs):
        pass

    def test(self, inputs):
        pass


class Test_Base_ELM(unittest.TestCase):

    def setup(self):
        """
        Creates an instance of the class that implements the required methods.
        """

    def teardown(self):
        """
        """
        pass

    def test_number_neurons(self):
        """
        Test the number of neurons argument.
        """
        assert_raises(TypeError, Base_ELM_Instance, number_neurons='Regis')
        assert_raises(TypeError, Base_ELM_Instance, number_neurons=12.3)
        assert_raises(ValueError, Base_ELM_Instance, number_neurons=-4)

    def test_linear_neurons(self):
        """
        Test the linear neurons argument.
        """
        assert_raises(TypeError, Base_ELM_Instance, linear_neurons=14)
        assert_raises(TypeError, Base_ELM_Instance, linear_neurons='Regis')

    def test_activation_function(self):
        """
        Test the activation function argument.
        """
        assert_raises(TypeError, Base_ELM_Instance, activation_function='Regis')
        assert_raises(TypeError, Base_ELM_Instance, activation_function=12)

    def test_gen_p_function(self):
        """
        Test the weight generation matrix function.
        """
        assert_raises(TypeError, Base_ELM_Instance, gen_p_function='Regis')
        assert_raises(TypeError, Base_ELM_Instance, gen_p_function=12)

if __name__ == "__main__":
    # Create a test suite
    elm_test_suite = unittest.TestSuite()

    # Add the right tests to the test suite
    elm_test_suite.addTest(Test_Base_ELM)

    # Run the test suite
    unittest.TextTestRunner().run(elm_test_suite)




#def main():
#    # # Run simple test functions
#    # numSamples = 1000
#    # numberNeurons = 200
#    # # Friedman data
#    # xtrain, ytrain, xtest, ytest = genFriedmanData(numSamples)
#    # myELM = ELM(numberNeurons=numberNeurons)
#    # ytrainh = myELM.train(xtrain, ytrain)
#    # ytesth = myELM.test(xtest)
#    # print 'Friedman Results for %s samples, %s neurons:' % (numSamples, myELM.numberNeurons)
#    # trainMSE, trainRMSE, trainMAE = errors(ytrainh, ytrain)
#    # testMSE, testRMSE, testMAE = errors(ytesth, ytest)
#    # print '\t Training:'
#    # print '\t\t MSE: %s  RMSE: %s  MAE: %s' % (trainMSE, trainRMSE, trainMAE)
#    # print '\t Testing:'
#    # print '\t\t MSE: %s  RMSE: %s  MAE: %s' % (testMSE, testRMSE, testMAE)
#    # # Other tests here
#    # myELM2 = ELM(numberNeurons=numberNeurons, rot=True)
#    # ytrainh = myELM2.train(xtrain, ytrain)
#    # ytesth = myELM2.test(xtest)
#    # print 'Friedman Results for %s samples, %s neurons:' % (numSamples, myELM2.numberNeurons)
#    # trainMSE, trainRMSE, trainMAE = errors(ytrainh, ytrain)
#    # testMSE, testRMSE, testMAE = errors(ytesth, ytest)
#    # print '\t\t MSE: %s  RMSE: %s  MAE: %s' % (trainMSE, trainRMSE, trainMAE)
#    # print '\t Testing:'
#    # print '\t\t MSE: %s  RMSE: %s  MAE: %s' % (testMSE, testRMSE, testMAE)#

#    # Go for UCI tests
#    # Abalone
#    numberNeurons = 100
#    trainMSE, trainRMSE, trainMAE, testMSE, testRMSE, testMAE, trainMSERot,\
#    trainRMSERot, trainMAERot, testMSERot, testRMSERot, testMAERot,\
#    trainMSERot2, trainRMSERot2, trainMAERot2, testMSERot2, testRMSERot2,\
#    testMAERot2 = compareELMToELMRot('/Users/ymiche/Work/UCI_Datasets/Regression/01-abalone.mat', numberNeurons)
#    printResults('01-abalone.mat', numberNeurons, trainMSE, trainRMSE, trainMAE, testMSE, testRMSE, testMAE, trainMSERot, trainRMSERot, trainMAERot, testMSERot, testRMSERot, testMAERot, trainMSERot2, trainRMSERot2, trainMAERot2, testMSERot2, testRMSERot2, testMAERot2)#

#    # Delta Ailerons
#    numberNeurons = 100
#    trainMSE, trainRMSE, trainMAE, testMSE, testRMSE, testMAE, trainMSERot,\
#    trainRMSERot, trainMAERot, testMSERot, testRMSERot, testMAERot,\
#    trainMSERot2, trainRMSERot2, trainMAERot2, testMSERot2, testRMSERot2,\
#    testMAERot2 = compareELMToELMRot('/Users/ymiche/Work/UCI_Datasets/Regression/02-delta_ailerons.mat', numberNeurons)
#    printResults('02-delta_ailerons.mat', numberNeurons, trainMSE, trainRMSE, trainMAE, testMSE, testRMSE, testMAE, trainMSERot, trainRMSERot, trainMAERot, testMSERot, testRMSERot, testMAERot, trainMSERot2, trainRMSERot2, trainMAERot2, testMSERot2, testRMSERot2, testMAERot2)#

#    # Delta Elevators
#    numberNeurons = 100
#    trainMSE, trainRMSE, trainMAE, testMSE, testRMSE, testMAE, trainMSERot,\
#    trainRMSERot, trainMAERot, testMSERot, testRMSERot, testMAERot,\
#    trainMSERot2, trainRMSERot2, trainMAERot2, testMSERot2, testRMSERot2,\
#    testMAERot2 = compareELMToELMRot('/Users/ymiche/Work/UCI_Datasets/Regression/03-delta_elevators.mat', numberNeurons)
#    printResults('03-delta_elevators.mat', numberNeurons, trainMSE, trainRMSE, trainMAE, testMSE, testRMSE, testMAE, trainMSERot, trainRMSERot, trainMAERot, testMSERot, testRMSERot, testMAERot, trainMSERot2, trainRMSERot2, trainMAERot2, testMSERot2, testRMSERot2, testMAERot2)#

#    # Computer Activity
#    numberNeurons = 100
#    trainMSE, trainRMSE, trainMAE, testMSE, testRMSE, testMAE, trainMSERot, trainRMSERot, trainMAERot, testMSERot, testRMSERot, testMAERot, trainMSERot2, trainRMSERot2, trainMAERot2, testMSERot2, testRMSERot2, testMAERot2 = compareELMToELMRot('/Users/ymiche/Work/UCI_Datasets/Regression/04-computer_activity.mat', numberNeurons)
#    printResults('04-computer_activity.mat', numberNeurons, trainMSE, trainRMSE, trainMAE, testMSE, testRMSE, testMAE, trainMSERot, trainRMSERot, trainMAERot, testMSERot, testRMSERot, testMAERot, trainMSERot2, trainRMSERot2, trainMAERot2, testMSERot2, testRMSERot2, testMAERot2)#

#    # Auto Price
#    # numberNeurons = 50
#    # trainMSE, trainRMSE, trainMAE, testMSE, testRMSE, testMAE, trainMSERot, trainRMSERot, trainMAERot, testMSERot, testRMSERot, testMAERot = compareELMToELMRot('06-auto_price.mat', numberNeurons)
#    # printResults('06-auto_price.mat', numberNeurons, trainMSE, trainRMSE, trainMAE, testMSE, testRMSE, testMAE, trainMSERot, trainRMSERot, trainMAERot, testMSERot, testRMSERot, testMAERot)#

#    # CPU
#    # numberNeurons = 30
#    # trainMSE, trainRMSE, trainMAE, testMSE, testRMSE, testMAE, trainMSERot, trainRMSERot, trainMAERot, testMSERot, testRMSERot, testMAERot = compareELMToELMRot('08-machine_cpu.mat', numberNeurons)
#    # printResults('08-machine_cpu.mat', numberNeurons, trainMSE, trainRMSE, trainMAE, testMSE, testRMSE, testMAE, trainMSERot, trainRMSERot, trainMAERot, testMSERot, testRMSERot, testMAERot)#

#    # Servo
#    #numberNeurons = 100
#    #trainMSE, trainRMSE, trainMAE, testMSE, testRMSE, testMAE, trainMSERot, trainRMSERot, trainMAERot, testMSERot, testRMSERot, testMAERot = compareELMToELMRot('09-servo.mat', numberNeurons)
#    #printResults('09-servo.mat', numberNeurons, trainMSE, trainRMSE, trainMAE, testMSE, testRMSE, testMAE, trainMSERot, trainRMSERot, trainMAERot, testMSERot, testRMSERot, testMAERot)#

#    # Breast Cancer
#    # numberNeurons = 100
#    # trainMSE, trainRMSE, trainMAE, testMSE, testRMSE, testMAE, trainMSERot, trainRMSERot, trainMAERot, testMSERot, testRMSERot, testMAERot = compareELMToELMRot('10-breast_cancer.mat', numberNeurons)
#    # printResults('10-breast_cancer.mat', numberNeurons, trainMSE, trainRMSE, trainMAE, testMSE, testRMSE, testMAE, trainMSERot, trainRMSERot, trainMAERot, testMSERot, testRMSERot, testMAERot)#

#    # Bank
#    #numberNeurons = 100
#    #trainMSE, trainRMSE, trainMAE, testMSE, testRMSE, testMAE, trainMSERot, trainRMSERot, trainMAERot, testMSERot, testRMSERot, testMAERot = compareELMToELMRot('11-bank.mat', numberNeurons)
#    #printResults('11-bank.mat', numberNeurons, trainMSE, trainRMSE, trainMAE, testMSE, testRMSE, testMAE, trainMSERot, trainRMSERot, trainMAERot, testMSERot, testRMSERot, testMAERot)#

#    # Stocks
#    #numberNeurons = 100
#    #trainMSE, trainRMSE, trainMAE, testMSE, testRMSE, testMAE, trainMSERot, trainRMSERot, trainMAERot, testMSERot, testRMSERot, testMAERot = compareELMToELMRot('13-stocks.mat', numberNeurons)
#    #printResults('13-stocks.mat', numberNeurons, trainMSE, trainRMSE, trainMAE, testMSE, testRMSE, testMAE, trainMSERot, trainRMSERot, trainMAERot, testMSERot, testRMSERot, testMAERot)#

#    # Boston
#    #numberNeurons = 100
#    #trainMSE, trainRMSE, trainMAE, testMSE, testRMSE, testMAE, trainMSERot, trainRMSERot, trainMAERot, testMSERot, testRMSERot, testMAERot = compareELMToELMRot('14-boston_housing.mat', numberNeurons)
#    #printResults('14-boston_housing.mat', numberNeurons, trainMSE, trainRMSE, trainMAE, testMSE, testRMSE, testMAE, trainMSERot, trainRMSERot, trainMAERot, testMSERot, testRMSERot, testMAERot)#
#

#if __name__ == '__main__':
#    main()
