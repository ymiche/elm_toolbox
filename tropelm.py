# -*- coding: utf-8 -*-
"""
TROP-ELM Module.

From Yoan Miche, Mark van Heeswijk, Patrick Bas, Olli Simula, Amaury Lendasse,
"TROP-ELM: A double-regularized ELM using LARS and Tikhonov regularization",
Neurocomputing, Volume 74, Issue 16, September 2011, Pages 2413-2421
doi:10.1016/j.neucom.2010.12.042

@author: ymiche
@version: 0.1
"""
